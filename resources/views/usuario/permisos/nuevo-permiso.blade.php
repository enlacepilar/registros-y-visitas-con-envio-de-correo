@extends('layouts.app-usuario')
@section('contenido_app')

<div class="container mt-4 animate__animated animate__lightSpeedInLeft">
    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif
    
    <div class="alert-info p-2">
        <h3 id="titulo">Permisos y Órdenes de Salida</h3>
        <hr>
        <h5>Usuario: {{$usuario->name}} | Correo-electrónico: {{$usuario->email}}</h5>
        <h6>Sector al que pertenece: <b>{{$usuario->departamento->nombre}}</b></h6>
    </div>

    <form action="/recibePermiso" class="form-group" method="POST" enctype="multipart/form-data">
    @csrf
        
        <input type="hidden" name="user_id" value="{{$usuario->id}}">
        <input type="hidden" name="departamento_id" value="{{$usuario->departamento_id}}">
    
        <label for="tipo_tramite" class="mt-4">Seleccione Tipo de permiso/orden de salida</label>
        <select name="tipo_tramite" id="tipo_tramite" class="form-control">
            <option value="Permiso de Salida">Permiso de Salida</option>
            <option value="Permiso de Ingreso Tardío">Permiso de Ingreso Tardío</option>
            <option value="Permiso de inasistencia">Permiso de inasistencia</option>
            <option value="Orden de Salida">Orden de Salida</option>
        </select>

    
        <label for="fecha_permiso" class="mt-4">Fecha: <i class="far fa-calendar-alt"></i></label>
        <input type="date" id="fecha_permiso" class="form-control" name="fecha_permiso" required>
        <div style="display:inline-block;"> @error('fecha_tramite') <span class="error text-danger">{{ $message }}</span> @enderror </div>
        
        <label for="motivo" class="mt-4">Motivo</label> 
        <textarea name="motivo" id="motivo" cols="5" rows="1" class="form-control"></textarea>

        <input type="submit" value="Enviar" class="form'control mt-3 btn btn-block btn-info">
    </form>

</div>
@endsection