@extends('layouts.app-usuario')
@section('contenido_app')
<div class="container mt-4 animate__animated animate__lightSpeedInLeft">
    <h1 id="titulo">Lista de permisos solicitados</h1>
    <a href="/nuevoPermiso"><button class="btn btn-primary mb-4">Nuevo Permiso</button></a>
    <table class="table table-info table-bordered table-hover" id="tabla_resultados">
        <thead>
            <tr>
                <th>Fecha Alta:</th>
                <th>Tipo de trámite</th>
                <th>Sector</th>
                <th>Archivo</th>
                <th>Fecha permiso</th>
                <th>Estado</th>
                <th>Modificar</th>
                <th>PDF</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tramites as $tramite )
            <tr>
                <td>{{date("d/m/Y", strtotime($tramite->created_at))}}</td>
                <td>{{$tramite->tipo_tramite}}</td>
                
                @if (isset($tramite->departamento->nombre))
                    <td>{{$tramite->departamento->nombre}}</td>  
                @else
                    <td>Sin sector</td>  
                @endif

                @if (isset($tramite->archivo_adjunto))
                <td><a href="{{$tramite->archivo_adjunto}}" target="_blank">Abrir archivo</a></td>
                @else
                <td>Sin archivo adjunto</td>
                @endif
                
                @if (isset($tramite->fecha_permiso))
                    <td>{{date("d/m/Y", strtotime($tramite->fecha_permiso))}}</td>
                @else
                    <td>Sin fecha cargada</td>
                @endif
               
                <td>{{$tramite->estado->nombre}}</td>

                @if ($tramite->estado_id == 3)
                    <td class="text-center"><button type="submit" class="btn btn-warning" disabled><i class="fas fa-edit"></i></button></td>
                @else
                    <form action="/modificaTramitePermisoAdmin">
                    @csrf
                        <input type="hidden" name="idTramite" value="{{$tramite->id}}">
                        <td class="text-center"><button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i></button></td>
                    </form>
                @endif
                <td><a href="/permisoPDF/{{$tramite->id}}"><button class="btn btn-success">Genera PDF</button></a></td>
            @endforeach   
                </tr>
            
        </tbody>
    
    </table>
</div>
<!-- <script>
let eliminaLibro = async (idLibro)=>
{
    console.log ("El resultado es" + idLibro)
    if (confirm("¿Seguro que querés borrar el libro?"))
    {
        const url = '/eliminaLibro'
        const request = new Request(url, 
        {
            method: 'POST',
            body: JSON.stringify({"idLibro": idLibro}),
            cache: "no-cache",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        
        });
        const response = await fetch(request);
        const data = await response.json();       
        console.log(data)
        Swal.fire(
            {
                type: 'success',
                title: data,
                showConfirmButton: false,
                timer: 1500
            })
        location.reload()
        
    }
    
        
}
</script> -->
@endsection
