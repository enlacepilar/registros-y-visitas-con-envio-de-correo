<nav class="navbar navbar-expand-lg navbar-light bg-info">
  <a class="navbar-brand  animate__animated animate__bounce" href="/"><img src="/imagenes/logo1.png" width="300px" class="img-fluid" alt=""></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav nav-tabs" id="items-navegacion">
    
        <li class="nav-item">
            <a class="nav-link" href="/datosUsuario" id="misDatos">Mis Datos</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/misLicencias" id="misLicencias">Mis Licencias</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/misPermisos" id="misPermisos">Mis Permisos</a>
        </li>
        
        </ul>
    </div>


      <!-- Barra a la derecha USUARIO -->
        <ul style="float:right;">            
        <!-- Authentication Links -->
            @guest
            @if (Route::has('login'))
                <li class="nav-item" style="float:left;">
                    <a class="nav-link" href="/login"><span class="text-light"><i class="fas fa-user-lock"></i> {{ __('login.login2') }}</span></a>
                </li>
                <li class="nav-item" style="float:right;">
                    <a class="nav-link" href="/register"><span class="text-light"><i class="fas fa-user-astronaut"></i> Registrarse</span></a>
                </li>
            @endif

            @else
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Usuario: {{ Auth::user()->name }}
                    </a>
                    
                    <div class="dropdown-menu animate__animated animate__fadeInUp" aria-labelledby="navbarDropdownMenuLink">
                        <!-- <a class="dropdown-item alert-warning" href="/datosUsuario">Editar mis datos</a> -->
                        <a class="dropdown-item alert-warning" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                           Salir
                        </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                    </div>
                </li>


            @endguest
        </ul>

       


</nav>









  