@extends('layouts.app-usuario')
@section('contenido_app')
<div class="container mt-4 animate__animated animate__lightSpeedInLeft">
    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif
    <h1 id="titulo">Datos del usuario</h1>
    <h4 class="alert-warning p-2">Contacte con el administrador para modificar datos.</h4>
    <form action="/modificaUsuario" method="POST">
    @csrf
        @foreach($usuario as $usu)
            <input type="hidden" name="id" value="{{$usu->id}}">
            
            <label for="nombre">Nombre completo:</label>
            <input type="text" id="nombre" name="nombre" class="form-control" disabled value="{{$usu->name}}"> 
            
            <br>
             
            <label for="direccion">Dirección:</label>
            <input type="text" id="direccion" name="direccion" class="form-control" disabled value="{{$usu->direccion}}"> 
            
            <br>

            <label for="socio">Legajo Nro.:</label>
            <input type="text" id="socio" name="socio" class="form-control" disabled value="{{$usu->socio}}"> 
            
            <br>

            <label for="correo">Correo:</label>
            <input type="text" id="correo" name="correo" class="form-control" disabled value="{{$usu->email}}"> 
        @endforeach
        <br>
        <!-- <input type="submit" value="Actualizar" class="btn btn-warning btn-block"> -->
    </form>
   
</div>
@endsection
