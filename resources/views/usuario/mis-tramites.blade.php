@extends('layouts.app-usuario')
@section('contenido_app')
<div class="container mt-4">
    <h1>Lista de trámites solicitados</h1>

    <div class="text-center"><a href="/" class="btn btn-outline-primary"><i class="fas fa-plus"></i> Nuevo trámite</a></div>

    <table class="table table-info table-bordered table-hover" id="tabla_resultados">
        <thead>
            <tr>
                <th>Fecha Alta:</th>
                <th>Tipo de trámite</th>
                <th>Sector</th>
                <th>Archivo</th>
                <th>Inicio licencia</th>
                <th>Fin</th>
                <th>Modificar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tramites as $tramite )
            <tr>
                <td>{{date("d/m/Y", strtotime($tramite->fecha_tramite))}}</td>
                <td>{{$tramite->tipo_tramite}}</td>
                @if (isset($tramite->departamento->nombre))
                    <td>{{$tramite->departamento->nombre}}</td>  
                @else
                    <td>Sin sector</td>  
                @endif
                @if (isset($tramite->archivo_adjunto))
                <td><a href="{{$tramite->archivo_adjunto}}" target="_blank">Abrir archivo</a></td>
                @else
                <td>Sin archivo adjunto</td>
                @endif
                
                @if (isset($tramite->licencia_desde))
                    <td>{{date("d/m/Y", strtotime($tramite->licencia_desde))}}</td>
                @else
                    <td>Sin fecha de inicio</td>
                @endif
               
                @if (isset($tramite->licencia_hasta))
                    <td>{{date("d/m/Y", strtotime($tramite->licencia_hasta))}}</td>
                @else
                    <td>Sin fecha de fin</td>
                @endif

            <form action="/modificaTramite">
            @csrf
                <input type="hidden" name="idTramite" value="{{$tramite->id}}"">
                <td class="text-center"><button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i></button></td>
            </form>
                <!-- <td class="text-center"><a href="/modificaTramite/{{$tramite->id}}" class="btn btn-outline-warning btn-"><i class="fas fa-edit"></i></a></td> -->
            @endforeach   
                </tr>
            
        </tbody>
    
    </table>
</div>

@endsection
