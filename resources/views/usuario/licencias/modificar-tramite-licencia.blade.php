@extends('layouts.app-usuario')
@section('contenido_app')
<div class="container mt-4 animate__animated animate__fadeIn">
    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif
    <h1 id="titulo">Modificar información de la licencia</h1>
    <form action="/recibeModificarTramiteLicencia" class="form-group" method="POST" enctype="multipart/form-data">
    @csrf
       
        <input type="hidden" name="idTramite" value="{{$tramite->id}}">
        
        <label for="fecha_tramite">Fecha de inicio de trámite y/o licencia</label>
        <input type="text" id="fecha_tramite" class="form-control" value="{{date('d/m/Y', strtotime($tramite->created_at))}}" disabled>
       
        <br>

        <label for="tipo_tramite">Fecha de inicio de trámite y/o licencia</label>
        <input type="text" class="form-control" value="{{$tramite->tipo_tramite}}" disabled>

        @if (isset($tramite->departamento->nombre))
            {{$tramite->departamento->nombre}}  
        @else
            Sin sector
        @endif
        
        <br>
        @if (isset($tramite->archivo_adjunto))
            <div class="alert-warning p-3"> <a href="{{$tramite->archivo_adjunto}}" target="_blank">Contiene archivo adjunto</a> </div>
        @else
            <!-- <label for="archivo_adjunto">Adjuntar archivo:</label>
            <input type="file" class="form-control bg-info" id="archivo_adjunto" name="archivo_adjunto"> -->
            <p class="alert-warning p-2">Sin adjunto. Aguarde hasta que el Administrador adjunte el archivo correspondiente.</p>
        @endif
        @error('archivo_adjunto') <span class="error text-danger">{{ $message }}</span> @enderror
        <br>
        
            <label for="licencia_desde">Licencia desde:</label>    
        @if ($tramite->licencia_desde != null)
            
            <input type="text" id="licencia_desde"  name="licencia_desde" class="form-control" value="{{date('d/m/Y', strtotime($tramite->licencia_desde))}}" disabled>
        @else
            <input type="date" id="licencia_desde" name="licencia_desde" class="form-control">
        @endif
        
        <br>      
        
        <label for="licencia_hasta">Licencia hasta:</label>    
        @if ($tramite->licencia_hasta != null)
            
            <input type="text" id="licencia_hasta" name="licencia_hasta" class="form-control" value="{{date('d/m/Y', strtotime($tramite->licencia_hasta))}}" disabled>
        @else
            <input type="date" id="licencia_hasta" name="licencia_hasta" class="form-control">
        @endif
        
        <br>
        <label for="motivo">Motivo:</label> 
        <textarea name="motivo" id="motivo" cols="2" rows="1" class="form-control">{{$tramite->motivo}}</textarea>

        <button type="submit" class="btn btn-warning btn-block">Actualizar datos</button>
        
    </form>
   
</div>
@endsection
