@extends('layouts.app-usuario')
@section('contenido_app')

<div class="container mt-4 animate__animated animate__lightSpeedInLeft">
    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif
    <div class="alert-info p-2">
        <h3 id="titulo">Nueva Licencia</h3>
        <hr>
        <h5>Usuario: {{$usuario->name}} | Correo-electrónico: {{$usuario->email}}</h5>        
        <h6>Sector al que pertenece: <b>{{$usuario->departamento->nombre}}</b></h6>
        <p>Puede completar las fechas de la licencia más adelante.</p>
    </div>

    <form action="/recibeLicencia" class="form-group" method="POST" enctype="multipart/form-data">
    @csrf
        
        <input type="hidden" name="user_id" value="{{$usuario->id}}">
        <input type="hidden" name="departamento_id" value="{{$usuario->departamento_id}}">
        

        <label for="tipo_tramite" class="mt-4">Seleccione Tipo de permiso/orden de salida</label>
        <select name="tipo_tramite" id="tipo_tramite" class="form-control">
            <option value="Licencia Anual">Licencia Anual</option>
            <option value="Licencia Especial">Licencia Especial</option>
   
        </select>

        <label for="licencia_desde" class="mt-4">Licencia desde:</label>        
        <input type="date" id="licencia_desde"  name="licencia_desde" class="form-control">

        <label for="licencia_hasta" class="mt-4">Licencia hasta:</label>        
        <input type="date" id="licencia_hasta"  name="licencia_hasta" class="form-control">

        <label for="motivo" class="mt-4">Motivo</label> 
        <textarea name="motivo" id="motivo" cols="5" rows="1" class="form-control"></textarea>
       
        <input type="submit" value="Enviar" class="form'control mt-3 btn btn-block btn-info">
    </form>

</div>
@endsection