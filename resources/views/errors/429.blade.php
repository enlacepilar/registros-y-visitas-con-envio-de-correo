@extends('errors::minimal')

@section('title', __('Demasiadas consultas'))
@section('code', '429')
@section('message', __('Demasiadas consultas. Espere un momento e intente nuevamente.'))
