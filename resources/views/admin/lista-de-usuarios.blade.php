@extends('layouts.app-admin')
@section('contenido_app')
<div class="container mt-4">
    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif
    <h1>Listado de usuarios registrados</h1>
    <a href="/nuevo_usuario"><button class="btn btn-info mb-2">Nuevo usuario</button></a>
  
    <div class="alert-warning p-4">
        <form action="/buscaUsuario" method="GET">
        @csrf
            <div class="form-row">
                <div class="col">
                    <input class="form-control" type="text" name="palabra" id="buscaUsuario" placeholder="Busque por nombre...">
                </div>
                <div class="col">
                    <!-- <button class="btn btn-info" type="submit" onclick="buscaUsuario()">Buscar</button> -->
                    <button class="btn btn-info" type="submit">Buscar</button>
                
                </div>
            </div>
        </form>
    </div>
   
    <table class="table alert-info table-bordered">
        <thead>    
            <th>Nombre</th>
            <th>Legajo</th>
            <th>Rol</th>
            <th>Sector</th>
            <th>Licencias</th>
            <th>Permisos</th>
            <th>Modificar datos</th>
            <th>Eliminar</th>
        </thead>
        <tbody id="bodyResultados">
  
        @foreach($usuarios as $usuario)
       
            <tr>            
                <td>{{$usuario->name}}</td>
                <td>{{$usuario->socio}}</td>
                <td>{{$usuario->rol->nombre}}</td>
                @if (isset($usuario->departamento->nombre))
                    <td>{{$usuario->departamento->nombre}}</td> 
                @else <td>Sin sector asignado</td>   
                @endif
                
            <form action="/adminLicenciasPorUsuario" method="post">
            @csrf
                <input type="hidden" value="{{$usuario->id}}" name="id">
                <td><button class="btn btn-outline-warning" type="submit">Licencias</i></button></td>
            </form>
            
            <form action="/adminPermisosPorUsuario" method="GET">
            @csrf
                <input type="hidden" value="{{$usuario->id}}" name="id"> 
                <td><button class="btn btn-outline-warning" type="submit">Permisos</i></button></td> 
            </form>
            <form action="/recibeDatosUsuarioAdmin" method="get">
            @csrf 
                <input type="hidden" value="{{$usuario->id}}" name="id">
                <td><button class="btn btn-outline-warning" type="submit"><i class="fas fa-user-edit"></i></button></td>                
            </form>

                <td><button class="btn btn-outline-danger" onclick="borraUsuario({{$usuario->id}})"><i class="fas fa-user-slash"></i></button></td>
            </tr>
       
    @endforeach
   
        </tbody>
    
    
    </table>
    

    @if ($usuarios->hasMorePages())
        {{$usuarios->links()}}
    @endif


</div>

{{-- #region Consultas sincronicas y asincronas con el servidor --}}
<script>
let enviaDatos = (ide) =>
{
    console.log(ide)
    window.location.href=`/editaUsuAdmin/${ide}`

}

let borraUsuario = async(idUsu) =>
{
    console.log(idUsu)
    if (confirm("¿Está seguro que desea eliminar el usuario? Esta operación no se puede revertir."))
    {
        const url = '/eliminaUsuario'
        const request = new Request(url, 
        {
            method: 'POST',
            body: JSON.stringify({"idUsu": idUsu}),
            cache: "no-cache",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        
        });
        const response = await fetch(request);
        const data = await response.text();
               
        console.log(data)
        Swal.fire({
            type: 'success',
            title: 'Usuario Eliminado',
            text: 'Aguarde un instante...',
            showConfirmButton: false
            })
            location.reload()
        
    }
    

}
</script>
{{-- #endregion --}}
@endsection