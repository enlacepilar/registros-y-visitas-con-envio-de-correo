@extends('layouts.app-admin')
@section('contenido_app')
<div class="container mt-4 alert-info p-3">
     
    <h1>Sectores</h1>
    <hr>
    @livewire('admin.departamentos-component')
</div>
@endsection