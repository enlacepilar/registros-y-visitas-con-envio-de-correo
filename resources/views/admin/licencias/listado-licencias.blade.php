@extends('layouts.app-admin')
@section('contenido_app')
<div class="container mt-4">

    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif

   
        <h1>Lista de licencias solicitadas por fecha y usuario</h1>
    

    <table class="table table-info table-bordered table-hover" id="tabla_resultados">
        <thead>
            <tr>
                <th>Fecha Alta:</th>
                <th>Usuario</th>
                <th>Tipo de trámite</th>
                <th>Sector</th>
                <th>Inicio licencia</th>
                <th>Fin</th>
                <th>Archivo</th>
                <th>Modificar</th>
                <th>Aprobar</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($tramites as $tramite )
            <tr>
                <td>{{date("d/m/Y", strtotime($tramite->created_at))}}</td>
                <td>{{$tramite->user->name}}</td>
                <td>{{$tramite->tipo_tramite}}</td>
                @if (isset($tramite->departamento->nombre))
                    <td>{{$tramite->departamento->nombre}}</td>  
                @else
                    <td>Sin sector</td>  
                @endif
                
                @if (isset($tramite->licencia_desde))
                    <td>{{date("d/m/Y", strtotime($tramite->licencia_desde))}}</td>
                @else
                    <td>Sin fecha de inicio</td>
                @endif
            
                @if (isset($tramite->licencia_hasta))
                    <td>{{date("d/m/Y", strtotime($tramite->licencia_hasta))}}</td>
                @else
                    <td>Sin fecha de fin</td>
                @endif
            
                @if (isset($tramite->archivo_adjunto))
                    <td><a href="{{$tramite->archivo_adjunto}}" target="_blank">Abrir archivo</a></td>
                @else
                    <td>Sin archivo adjunto</td>
                @endif 
                
                @if (isset($tramite->archivo_adjunto))
                    <td><button class="btn btn-warning" disabled>Modificar</button></td>
                @else
                    <td>
                        <form action="/actualizaLicenciaAdmin" method="get">
                            <input type="hidden" value="{{$tramite->id}}" name="idTramite">
                            <button type="submit" class="btn btn-warning">Modificar</button>
                        </form>
                    </td>
                @endif 

                @if ($tramite->estado_id == 1)
                    <td><button class="btn btn-success" onclick="aprobarLicencia({{$tramite->id}})">Aprobar</button></td>
                @elseif ($tramite->estado_id == 2)
                    <td><b><i>Aprobada</i></b></td>
                @else
                    <td>{{$tramite->estado->nombre}}</td>
                @endif
                
            @endforeach   
                </tr>
        </tbody>
    </table>
</div>
@endsection

<!-- Llamo al archivo para la consulta asicrona -->
<script src="/js/aprobarLicencia.js"></script>