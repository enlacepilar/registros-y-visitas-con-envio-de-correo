@extends('layouts.app-admin')
@section('contenido_app')
<div class="container mt-4">
    <h1>Listado de trámites registrados</h1>

    <table id="tabla_resultados" class="table table-info">
        <thead>
            <th>Usuario</th>
            <th>Correo</th>
            <th>Nro. socio</th>
            <th>Sector</th>
            <th>Tramite</th>
            <th>Fecha tramite</th>
            <th>Licencia desde</th>
            <th>Licencia hasta</th>
            <th>Archivo adjunto</th>
            <th>Aprobar</th>


        </thead>
        @foreach ($tramites as $tramite)
            <tr>
                <td>{{$tramite->nombre}}</td>
                <td>{{$tramite->correo}}</td>
                <td>{{$tramite->socio}}</td>
                @if (isset($tramite->departamento->nombre))
                    <td>{{$tramite->departamento->nombre}}</td>  
                @else
                    <td>Sin sector</td>  
                @endif
                
                <td>{{$tramite->tramite}}</td>
                <td>{{$tramite->fecha_tramite}}</td>
                <td>{{$tramite->licencia_desde}}</td>
                <td>{{$tramite->licencia_hasta}}</td>
                @if ($tramite->archivo_adjunto)
                <td><a href="{{$tramite->archivo_adjunto}}" target=_blank>Archivo adjunto</a></td>
                @else
                <td>Sin adjunto</td>
                @endif
                <td><button class="btn btn-primary" onclick="aprobar({{$tramite->idLicencia}})">Aprobar</button></td>
            </tr>
        
        @endforeach
    </table>

</div>

<script>
    let aprobar = (ide) =>
    {
        console.log ('Aprobar tramite: ' + ide)
        window.location.href=`/aprobarLicencia/${ide}`
    }
</script>
    @endsection

