@extends('layouts.app-admin')
@section('contenido_app')
<div class="container mt-4">
    <h1>Lista de permisos solicitados</h1>

    <table class="table table-info table-bordered table-hover" id="tabla_resultados">
        <thead>
            <tr>
                <th>Fecha Alta:</th>
                <th>Tipo de trámite</th>
                <th>Sector</th>
                <th>Archivo</th>
                <th>Modificar</th>
                <th>Aprobar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tramites as $tramite )
            <tr>
                <td>{{date("d/m/Y", strtotime($tramite->created_at))}}</td>
                <td>{{$tramite->tipo_tramite}}</td>
                
                @if (isset($tramite->departamento->nombre))
                    <td>{{$tramite->departamento->nombre}}</td>  
                @else
                    <td>Sin sector</td>  
                @endif


                @if (isset($tramite->archivo_adjunto))
                <td><a href="{{$tramite->archivo_adjunto}}" target="_blank">Abrir archivo</a></td>
                @else
                <td>Sin archivo adjunto</td>
                @endif
                
                <form action="/modificaTramitePermisoAdmin">
                @csrf
                <input type="hidden" name="idTramite" value="{{$tramite->id}}"">
                <td class="text-center"><button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i></button></td>
            </form>
                @if ($tramite->estado_id == 1)
                    <td><button class="btn btn-success" onclick="aprobarPermiso({{$tramite->id}})">Aprobar</button></td>
                @elseif ($tramite->estado_id == 2)
                    <td><b><i>Aprobado</i></b></td>
                @else
                    <td>{{$tramite->estado->nombre}}</td>
                @endif
            @endforeach   
                </tr>
            
        </tbody>
    
    </table>
</div>
@endsection
<!-- Llamo al archivo para la consulta asicrona -->
<script src="/js/aprobarPermiso.js"></script>
