@extends('layouts.app-admin')
@section('contenido_app')
<div class="container mt-4 alert-info p-3">
     
    <h1>Nuevo usuario</h1>
    @livewire('admin.crear-usuario-component')
</div>
@endsection