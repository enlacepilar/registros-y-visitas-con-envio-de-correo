<div>
    <h1 class="text-primary animate__animated animate__fadeIn">{{$titulo}}</h1>
    <p>Al cargar el archivo se cierra automáticamente el trámite</p>
    
    <div class="m-3">
        <button class="btn btn-info mr-3" wire:click="aprobados">Aprobados</button>    
        <button class="btn btn-dark" wire:click="cerrados">Cerrados</button>    
    </div>

    @if (session()->has('mensajeOk'))

        <div class="animate__animated animate__flash alert alert-danger text-success text-center">
            <h3><b>{{ session('mensajeOk') }}</b></h3>
        </div>
    @endif

    @if (count($tramites)>0)
        <table class="table table-info table-bordered table-hover">
            <thead class="alert-info">
                <tr>
                    <th>Fecha Alta:</th>
                    <th>Usuario</th>
                    <th>Tipo de licencia</th>
                    <th>Sector</th>
                    <th>Archivo</th>
                    <th>Cargar permiso y cerrar trámite</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tramites as $tramite )
                <tr>
                    <td>{{date("d/m/Y", strtotime($tramite->created_at))}}</td>
                    <td>{{$tramite->user->name}}</td>
                    <td>{{$tramite->tipo_tramite}}</td>
                    @if (isset($tramite->departamento->nombre))
                        <td>{{$tramite->departamento->nombre}}</td>  
                    @else
                        <td>Sin sector</td>  
                    @endif
                    
                                    
                    @if (!isset($tramite->archivo_adjunto))
                        <td> <input type="file" name="archivo" wire:model="archivo"></td>
                    @elseif (isset($tramite->archivo_adjunto))
                        <td><a href="{{$tramite->archivo_adjunto}}" target="_blank">Abrir archivo</a></td>
                    @else
                        <td>Hubo un error, no hay archivo adjunto.</td>
                    @endif 
                    
                    @if (isset($tramite->archivo_adjunto))
                        <td><i>Trámite cerrado.</i></td>
                    @else
                        <td><button class="btn btn-warning" wire:click="subirArchivoPermiso({{$tramite->id}})"><i class="fas fa-cloud-upload-alt"></i></button></td>
                    @endif

                @endforeach   
                    </tr>
            </tbody>
        </table>
        <div class="text-center">@error('archivo') <span class="error text-center text-danger p-3 mb-5">{{ $message }}</span> @enderror</div>
        
        {{$tramites->links()}}
    @else
        <h5>Sin resultados</h5>
    @endif
</div>
