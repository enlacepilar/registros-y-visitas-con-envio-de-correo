<div class='container alert-warning'>
    
    @if ($activo_mensaje == "si")
        @if ($mensaje=="")
            <p></p>    
        @elseif ($mensaje=="ok")
            <p class="p-1 alert-success animate__animated animate__bounceInRight text-center">Turno cargado correctamente.</p>
        
        @else
            <p class="p-1 alert-danger animate__animated animate__bounceInRight">{{$mensaje}}</p>
        @endif 
    @endif
<!-- 
    <div class="row mb-3">
        <div class='col-5'>
            <label class="display-block" for="turno">Especialidad</label>
            <select class="form-control" id="turno" wire:model="turno">
                <option value="seleccione">Seleccione especialidad</option>
                <option value="cardiologia">Cardiologia</option>
                <option value="pediatria">Pediatria</option>
                <option value="traumatologia">Traumatologia</option>
            </select>
        </div>
    </div> -->


    <label class="display-block" for="fecha">Seleccione un dia para el turno</label>
    <div class="row p-2">
        <div class='col-5'>
            <input class="form-control" type="date" wire:model="fecha" id="fecha">
        </div>
        
        <div class='col-5'>
            <button class="btn btn-secondary" wire:click="buscaHorario">Continuar</button>
        </div>
    </div>

@if ($activo_horario == "si")
    <label class="display-block mt-2" for="hora">Seleccione un horario disponible</label>
    <div class="row pl-2 pr-2 pb-2 ">
        <div class='col-5'>
            <select class="form-control" name="hora" id="hora" wire:model.defer="hora">
            <option value="seleccione">Seleccione</option>
                @foreach ($horarios as $hora)
                    <option value="{{$hora}}">{{$hora}}</option>
                @endforeach
            </select>
        </div>
        
        <div class="col-5">
            <button class="btn btn-info"wire:click="crearTurno">Generar turno</button>
        </div>
    
    </div>
@endif
   


   
</div>
