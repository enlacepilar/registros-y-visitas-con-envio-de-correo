<div>





    <label for="nombre">Nombre completo:</label>
    <input type="text" class="form-control" id="nombre" wire:model="nombre" required> 
    
    <br>
        
    <label for="direccion">Dirección:</label>
    <input type="text" id="direccion" wire:model="direccion" class="form-control" required> 
    
    <br>

    <label for="telefono">telefono Nro.:</label>
    <input type="text" id="telefono" wire:model="telefono" class="form-control" required> 
    
    <br>

    <label for="dni">DNI Nro.:</label>
    <input type="text" id="dni" wire:model="dni" class="form-control" required> 
    
    <br>

    <label for="cuil">Cuil Nro.:</label>
    <input type="text" id="cuil" wire:model="cuil" class="form-control" required> 
    
    <br>

    <label for="legajo">Legajo Nro.:</label>
    <input type="text" id="legajo" wire:model="legajo" class="form-control" required> 
    
    <br>

    <label for="correo">Correo:</label>
    <input type="text" id="correo" wire:model="correo" class="form-control" required> 
    
    <br>
    
    <label for="sector">Sector:</label>
    <select wire:model="sector" id="sector" class="form-control">
        <option value="" selected>Seleccione</option>
        @foreach($departamentos as $departamento)
            <option value="{{$departamento->id}}">{{$departamento->nombre}}</option>
        @endforeach
    </select>

    <br>
    
    <label for="rol">Rol:</label>
    <select wire:model="rol" id="rol" class="form-control">
        <option value="" selected>Seleccione</option>
        @foreach($roles as $rol)
        <option value="{{$rol->id}}">{{$rol->nombre}}</option>
    @endforeach
    </select>

    <br>

    <label for="clave">Clave:</label>
    <input type="password" id="clave" wire:model="clave" class="form-control" required> 

    <br>
    @if (session()->has('mensajeError'))

            <div class="alert alert-danger">

                {{ session('mensajeError') }}

            </div>
    @elseif (session()->has('mensajeOk'))

                <div class="animate__animated animate__flash alert alert-dark text-success text-center">

                    <h3><b>{{ session('mensajeOk') }}</b></h3>

                </div>
    @elseif (session()->has('mensajeAdvertencia'))

        <div class="alert alert-warning">

            {{ session('mensajeAdvertencia') }}

        </div>
    @elseif (session()->has('mensajeAdvertenciaRol'))

        <div class="alert alert-warning">

            {{ session('mensajeAdvertenciaRol') }}

        </div>
    @else
    <div></div>

    @endif
    
    <button class="btn btn-primary btn-block" wire:click="recibeDatosUsuario">Nuevo usuario</button>


</div>
