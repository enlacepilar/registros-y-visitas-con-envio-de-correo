<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- bootstrap --}}
    <link rel="stylesheet" href="bootstrap-local/css/bootstrap.min.css">
    
    <title>Migraciones</title>
</head>
<body>
    <div class="container m-5">
        <h1>Antes de empezar...</h1>

        <a href="/crear-roles" class="btn btn-primary ml-3">Crear Roles de usuario</a>

        <a href="/crear-estados" class="btn btn-primary">Crear Estados del tramite</a>

        <a href="/crear-departamentos" class="btn btn-primary">Crear Departamentos</a>

    </div>

    <!-- scripts a Jquery, bootstrap -->
    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="bootstrap-local/js/bootstrap.min.js"></script>
</body>
</html>