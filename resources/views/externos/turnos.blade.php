@extends('layouts.app-externos')
@section('contenido_app')
<div class="container mt-4">
    <h1>Solicitar turno</h1>

    <p>Seleccione la fecha deseada y un horario disponible.</p>
</div>
@livewire('externos.turnos-component')
@endsection
