<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/genera-pdf.css">
    <title>Osdem Permisos</title>
</head>
<body>
<div  style="background-color:white;" class="container">
    <div><img src="imagenes/logo-osdem-pdf.png" width="400px" alt="Logo Osdem"> 
    <div id='class='mt-2 mb-2'>
            <table id='tabla_interna'>
                <tr><td colspan="2" id='titulo_formu'>{{$permiso->tipo_tramite}}</td></tr>
                <tr><td id='items-firma1'>Generado el día: </td><td id='items-firma2'>{{date("d/m/Y", strtotime($permiso->created_at))}}</td></tr>
                <tr><td colspan="2" id='titulos_internos'>Datos del empleado:</td></tr>
               
                <tr><td id='items-firma1'>Departamento: </td><td id='items-firma2'>{{$permiso->departamento->nombre}}</td></tr>
             
                <tr><td id='items-firma1'>Nombre completo: </td><td id='items-firma2'>{{$permiso->user->name}}</td></tr>
                <tr><td id='items-firma1'>Legajo: </td><td id='items-firma2'>{{$permiso->user->socio}}</td></tr>
                <tr><td id='items-firma1'>Correo: </td><td id='items-firma2'>{{$permiso->user->email}}</td></tr>
                <tr><td id='items-firma1'>Fecha Permiso: </td><td id='items-firma2'>{{date("d/m/Y", strtotime($permiso->fecha_permiso))}}</td></tr>
                               
                <tr><td colspan="2"  id='titulos_internos'>Motivo de la solicitud:</td></tr>
                <tr><td colspan="2" id='problema1'><i>{{$permiso->motivo}}</i></td></tr>
                <div id='problema'> </div>
                <tr><td colspan="2" id='titulos_internos'>Autorización:</td></tr>
            </table>
            <table id='tabla_formu'>
                <tr><th id='items-firma3'>Firma y Fecha Superior: </th><th  id='items-firma3'>Firma Empleado:</th></tr>
                <tr><td id='espacio-firma'></td><td></td></tr>
            </table>

    
    </div>
</div>




</body>
</html>
