<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'rol_id',
        'direccion',
        'telefono',
        'dni',
        'cuil',
        'socio',
        'email',
        'departamento_id',
        'password',
        
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function turnos ()
    {
        return $this->hasMany ('App\Models\Turno');
    }

    public function permisos ()
    {
        return $this->hasMany ('App\Models\Permiso');
    }

    public function licencias ()
    {
        return $this->hasMany ('App\Models\Licencia');
    }

    public function departamento ()
    {
        return $this->belongsTo ('App\Models\Departamento');
    }

    public function rol ()
    {
        return $this->belongsTo ('App\Models\Rol');
    }
}
