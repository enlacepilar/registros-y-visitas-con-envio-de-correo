<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    use HasFactory;

    public function users ()
    {
        return $this->hasMany ('App\Models\User');
    }

    public function licencia ()
    {
        return $this->hasMany ('App\Models\Licencia');
    }

    public function permiso ()
    {
        return $this->hasMany ('App\Models\Permiso');
    }
}
