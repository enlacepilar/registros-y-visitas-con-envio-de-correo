<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Departamento;

class DepartamentoController extends Controller
{
    public function crearDepartamentos()
    {
        $departamenos = Departamento::all();

        if (count($departamenos)<1)
        {
            $departamento = new Departamento;
            $departamento->nombre = "Mesa de Entradas";
            $departamento->save();

            $departamento = new Departamento;
            $departamento->nombre = "Afiliaciones";
            $departamento->save();

            $departamento = new Departamento;
            $departamento->nombre = "Recursos Humanos";
            $departamento->save();

            $departamento = new Departamento;
            $departamento->nombre = "Institucional";
            $departamento->save();

            $departamento = new Departamento;
            $departamento->nombre = "Contable";
            $departamento->save();

            $departamento = new Departamento;
            $departamento->nombre = "Facturacion y Auditoria";
            $departamento->save();

            $departamento = new Departamento;
            $departamento->nombre = "Gerencia Prestacional";
            $departamento->save();

            $departamento = new Departamento;
            $departamento->nombre = "Gerencia Contable";
            $departamento->save();

            $departamento = new Departamento;
            $departamento->nombre = "Gerencia General";
            $departamento->save();

            echo "Sectores creados exitosamente";
        }
        else{
            echo "Los sectores ya fueron creados anteriormente.";
        }
    }
}
