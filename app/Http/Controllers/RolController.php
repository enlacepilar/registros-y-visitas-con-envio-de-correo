<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol;

class RolController extends Controller
{
    public function crearRoles()
    {
        $roles = Rol::all();

        if (count($roles)<1)
        {
            $rol = new Rol;
            $rol->nombre = "Administrador";
            $rol->save();

            $rol = new Rol;
            $rol->nombre = "Usuario";
            $rol->save();

            $rol = new Rol;
            $rol->nombre = "Recursos Humanos";
            $rol->save();

            $rol = new Rol;
            $rol->nombre = "Usuarios Externos";
            $rol->save();

            echo "Roles creados exitosamente";
        }
        else{
            echo "Los roles ya fueron creados anteriormente.";
        }
    }
}
