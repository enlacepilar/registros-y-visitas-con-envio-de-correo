<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estado;

class EstadoController extends Controller
{
    public function crearEstados()
    {
        $estados = Estado::all();

        if (count($estados)<1)
        {
            $estado = new Estado;
            $estado->nombre = "Iniciado";
            $estado->save();

            $estado = new Estado;
            $estado->nombre = "Aprobado";
            $estado->save();

            $estado = new Estado;
            $estado->nombre = "Cerrado";
            $estado->save();

            echo "Datos creados exitosamente";
        }
        else{
            echo "Los estados ya fueron creados";
        }
    }
}
