<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class IngresoController extends Controller
{

    //1 admin 2 usuario 3 recursos humanos 4 externos

    public function ingreso()
    {
        $usuarioID = auth()->id();
        //$usu = Session::all();
        $usuario = User::find($usuarioID);
        if ($usuario->rol_id == 1)
        {            
            return redirect()->action([AdministradorController::class, 'adminDatosUsuario']);
        }
        elseif ($usuario->rol_id == 2)
        {
            //return redirect()->action([UsuarioController::class, 'inicio']);
            return redirect()->action([UsuarioController::class, 'inicio']);
        }
        elseif ($usuario->rol_id == 3)
        {
            
            return redirect()->action([RecursosHumanosController::class, 'licencias']);
        }
        elseif ($usuario->rol_id == 4)
        {
            
            return view ('externos.turnos', ['usuario'=>$usuario] );
        }
        else
        {
            return "No tiene un rol válido";
        }
    }
}
