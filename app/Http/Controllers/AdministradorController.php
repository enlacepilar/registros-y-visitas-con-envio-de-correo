<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Licencia;
use App\Models\Permiso;
use App\Models\Departamento;
use App\Models\Rol;
use Illuminate\Support\Facades\DB;

class AdministradorController extends Controller
{
    public function adminDatosUsuario ()
    {
        
        $idAdmin = auth()->id();
        $usuAmin = User::find($idAdmin);
        
        if (isset($usuAmin->departamento_id))
        {
            $usuarios = User::where('departamento_id', $usuAmin->departamento_id)->paginate(15);     
        }else
        {
            $usuarios = User::paginate(15);  
        }

       

        return view ('admin.lista-de-usuarios', ['usuarios'=> $usuarios]);
    }

    public function recibeDatosUsuarioAdmin (Request $request)
    {
        $id = $request->get('id');
        $usu = User::find($id);
        $departamentos = Departamento::all();
        $roles = Rol::all();
        return view ('admin.modifica-usuario', ['usu'=> $usu, 'departamentos'=>$departamentos, 'roles'=>$roles]);
    }

    public function buscaUsuario (Request $request)
    {
        
        $palabra = $request->get('palabra');
       
        $usuarios= User::where('name', 'LIKE', "%$palabra%")->paginate(15);
        
        $cuenta = (count($usuarios));
        //ver para que cuenta muestre o no resultados
        return view ('admin.lista-de-usuarios', ['usuarios'=> $usuarios]);
    }

    public function editaUsuAdmin ($ide)
    {
        $usu = User::find($ide);
               
        return view ('admin.modifica-usuario-individual', ['usu'=> $usu]);    

    }

    public function modificaUsuarioAdmin (Request $request)
    {
        if ($id = $request->get('id'))
        {
            $id = $request->get('id');
            $usuarioModificado = $request->get('nombre');
            
            $actualizaUsu = User::find($id);
            $actualizaUsu->name = $request->get('nombre');
            $actualizaUsu->direccion = $request->get('direccion');
            $actualizaUsu->telefono = $request->get('telefono');
            $actualizaUsu->dni = $request->get('dni');
            $actualizaUsu->cuil = $request->get('cuil');
            $actualizaUsu->socio = $request->get('socio');
            $actualizaUsu->email = $request->get('correo');
            if (($request->get('sector')) != "")
            {
                $actualizaUsu->departamento_id = $request->get('sector');
            }
            
            if (($request->get('rol')) != "")
            {
                $actualizaUsu->rol_id = $request->get('rol');
            }
            
            $actualizaUsu->save(); 

            $datos = "Datos del usuario $usuarioModificado actualizados";

            $usuarios = User::paginate(15);
            return view ('admin.lista-de-usuarios', ['usuarios'=> $usuarios, 'datos'=>$datos]);
        }else
        {
            $usuarios = User::paginate(15);
            return view ('admin.lista-de-usuarios', ['usuarios'=> $usuarios]);
        }
    }

    public function eliminaUsuario (Request $request)
    {
        $idUsu = $request->json('idUsu');
        
        $usuario = User::find($idUsu);
        $usuario->delete();

        $mensaje = "Usuario eliminado.";
        return response()->json( [$mensaje] );
    }
    
    public function adminTramites ()
    {
        $tramites = DB::table('licencias as lic')
        ->join('users AS usu', 'lic.id_usuario', '=', 'usu.id')
        ->select('lic.tipo_tramite AS tramite', 'lic.sector_pertenencia AS sector', 'lic.created_at',
        'lic.licencia_desde', 'lic.licencia_hasta', 'lic.archivo_adjunto', 
        'usu.name AS nombre', 'usu.email AS correo', 'usu.socio AS socio')
        ->get();

        return view ('admin.lista-de-tramites', ['tramites'=> $tramites]);
    }
   
}
