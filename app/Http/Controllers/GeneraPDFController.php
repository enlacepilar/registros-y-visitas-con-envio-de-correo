<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permiso;
use App\Models\Licencia;
use PDF;

class GeneraPDFController extends Controller
{
    public function permisos($idPermiso)
    {
        $permiso = Permiso::find($idPermiso);
        $pdf = PDF::loadView('generaPdf.permisos', ['permiso'=>$permiso]);
      
        return $pdf->stream('permiso.pdf');
    }

    public function licencias($idLicencia)
    {
        $licencia = Licencia::find($idLicencia);
        $pdf = PDF::loadView('generaPdf.licencias', ['licencia'=>$licencia]);
       
        return $pdf->stream('licencia.pdf');
    }
}

