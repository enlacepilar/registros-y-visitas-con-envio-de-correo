<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class UsuarioController extends Controller
{   
    public function inicio ()
    {
        dd(env('APP_NAME'));
        return "hola";
    }

    public function datosUsuario ()
    {
        $usuarioID = auth()->id();
        $usuario = User::where('id', $usuarioID)->get();
        return view('usuario.datos.modificar-datos-usuario', ['usuario'=>$usuario]);
    }

    public function modificaUsuario (Request $request)
    {
        $id = $request->get('id');
        
        $actualizaUsu = User::find($id);
        $actualizaUsu->name = $request->get('nombre');
        $actualizaUsu->direccion = $request->get('direccion');
        $actualizaUsu->socio = $request->get('socio');
        $actualizaUsu->email = $request->get('correo');
        $actualizaUsu->save(); 

        $usuarioID = auth()->id();
        $usuario = User::where('id', $usuarioID)->get();
        return view('usuario.datos.modificar-datos-usuario', ['usuario'=>$usuario, 'datos'=>"Datos actualizados."]);
        
    }


    
}
