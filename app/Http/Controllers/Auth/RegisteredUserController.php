<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

use App\Models\Rol;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $roles =  Rol::all();
        if (count($roles)<1)
        {
            $user = User::create([
                'name' => $request->name,
                'direccion' =>$request->direccion,
                'telefono' =>$request->telefono,
                'dni' =>$request->dni,
                'cuil' =>$request->cuil,
                'socio'=>$request->socio,
                'email' => $request->email,
                'password' => Hash::make($request->password),
        
            ]);
        }else
        {
            $user = User::create([
                'name' => $request->name,
                'direccion' =>$request->direccion,
                'telefono' =>$request->telefono,
                'dni' =>$request->dni,
                'cuil' =>$request->cuil,
                'socio'=>$request->socio,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'rol_id'=>'2',
            ]);
        }    

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
