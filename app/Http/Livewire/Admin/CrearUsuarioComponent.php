<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Departamento;
use App\Models\Rol;

class CrearUsuarioComponent extends Component
{
    public $datos, $mensaje;
    
    //Datos del formulario
    public $nombre, $direccion, $telefono, $dni, $cuil, $legajo, $correo, $sector, $rol, $clave;
    
    public function render()
    {
        $departamentos =Departamento::all();
        $roles = Rol::all();
        return view('livewire.admin.crear-usuario-component', ['departamentos'=>$departamentos, 'roles'=>$roles]);
    }

    public function recibeDatosUsuario()
    {
                
        if (User::where('email', $this->correo)->exists())
        {
            
            session()->flash('mensajeError', 'El email ya existe en la base.');
            
        }elseif(
            ($this->nombre == "") ||
            ($this->direccion == "") ||
            ($this->telefono == "") ||
            ($this->dni == "") ||
            ($this->cuil == "") ||
            ($this->legajo == "") ||
            ($this->correo == "") ||
            ($this->sector == "") ||
            ($this->clave == "")
        )
        {
            session()->flash('mensajeAdvertencia', 'Complete todos los campos.');
        }
        elseif(($this->rol == "") || ($this->rol == null))
        {
            session()->flash('mensajeAdvertenciaRol', 'Debe asigar un rol.');
        } 
        
        
        else{
            $fecha = date('Y-m-d H:i:s');
            $this->clave = Hash::make($this->clave);
            
            $usuarioNuevo = new User;
            $usuarioNuevo->name = $this->nombre;
            $usuarioNuevo->direccion = $this->direccion;
            $usuarioNuevo->telefono = $this->telefono;
            $usuarioNuevo->dni = $this->dni;
            $usuarioNuevo->cuil = $this->cuil;
            $usuarioNuevo->socio = $this->legajo;
            $usuarioNuevo->email = $this->correo;
            $usuarioNuevo->departamento_id = $this->sector;
            $usuarioNuevo->password = $this->clave;
            $usuarioNuevo->created_at = $fecha;
            $usuarioNuevo->rol_id = $this->rol;
            $usuarioNuevo->save();

            session()->flash('mensajeOk', 'Usuario creado.');
        
            
        }
    }
}
