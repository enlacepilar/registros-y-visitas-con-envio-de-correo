<?php

namespace App\Http\Livewire\Externos;

use Livewire\Component;
use App\Models\Turno;
use App\Models\User;

class TurnosComponent extends Component
{
    public $horarios;
    public $especialidad, $fecha_original, $fecha, $hora, $usuarioID, $turno;
    public $resultado; // esto sirve para activar o desactivar los horarios en la vista
    public $mensaje ="";
    public $activo_horario = "no";
    public $activo_mensaje = "no";
    
    //public $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
    //pruebas variables
    public $dia, $esta_la_hora;

    
    public function render()
    {
          $this->usuarioID = auth()->id();

          //verifico que el resultado cambie. Si queres podes poner una variavble en la 
          //vista turnos componente para comprobarlo como lo hice yo. Sirve para ocultar el cambio de fecha 
          if( $this->fecha_original == $this->fecha)
          {
               $this->resultado = "iguales";
          }
          else{ 
               $this->resultado = "distintas";
               $this->fecha_original = $this->fecha;
               $this->activo_mensaje = "no";
               $this->activo_horario= "no";
          }        

          return view('livewire.externos.turnos-component');         
    }

    public function buscaHorario() 
    {
         
          $turnos = Turno::where('fecha_turno', $this->fecha)->get(); //obtengo todas las fechas segun el dia
          $this->activo_horario= "si";
          $this->activo_mensaje= "no";

          $this->horarios = ['9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '14:00', '14:30', '15:00'];

          //Verifico si los horarios existen en el array
          foreach ($turnos as $turno)
          {
               if (in_array($turno->hora, $this->horarios))
               {
                    $posicion = array_search($turno->hora, $this->horarios);
                    unset($this->horarios[$posicion]);
                    $this->esta_la_hora = "Si esta la hora";
               }else
               {
                    $this->esta_la_hora = "No hay";
               }
          }

    }

    public function crearTurno()
    {   
     $this->activo_mensaje= "si";

        if (($this->fecha == "") || ($this->hora== "") || ($this->hora== "seleccione") )
          // || ($this->turno== "Seleccione especialidad") || ($this->turno== ""))
       {
          $this->mensaje = "Faltan datos para poder cargar el turno.";
       }
       elseif (($this->fecha != "") || ($this->hora!= "") || ($this->hora!= "seleccione"))
       {
          $i = strtotime($this->fecha);
          $this->dia= jddayofweek(cal_to_jd(CAL_GREGORIAN, date("m",$i),date("d",$i), date("Y",$i)) , 0 ); //ordena por dia de semana 0 es domingo
          
          if (($this->dia == 6) || ($this->dia == 0))
          {
               $this->mensaje = "No se puede solicitar turno sabado o domingo";
          }else{
               $turno = new Turno;
               $turno->user_id = $this->usuarioID;
               $turno->turno = "Turno para SEDE";// por el momento esto no va $this->turno;
               $turno->fecha_turno = $this->fecha;
               $turno->hora = $this->hora;
               $turno->save();
               $this->mensaje = "ok";
          }
       }
       else
       {
          $this->mensaje = "";
       }
    }
}