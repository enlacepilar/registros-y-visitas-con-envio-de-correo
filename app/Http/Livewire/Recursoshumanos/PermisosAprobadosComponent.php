<?php

namespace App\Http\Livewire\Recursoshumanos;

use Livewire\Component;
use App\Models\Permiso;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class PermisosAprobadosComponent extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $paginationTheme = "bootstrap";

    //Ver tabla estados.
    public $estado = 2;
    public $titulo = "Permisos Aprobados";
    public $archivo;

    public function render()
    {
        $permisos = Permiso::Where('estado_id', $this->estado)->latest()->Paginate(20);
       
        return view('livewire.recursoshumanos.permisos-aprobados-component',['tramites'=>$permisos]);
    }

    public function cerrados()
    {
        $this->estado = 3;
        $this->titulo = "Permisos Cerrados";
    }

    public function aprobados()
    {
        $this->estado = 2;
        $this->titulo = "Permisos Aprobados";
    }

    function subirArchivoPermiso ($idePermiso)
    {
        $this->validate(
            [
            'archivo' => 'required|mimes:pdf|max:5000', // 5MB Max
        ]);
           
        if (($this->archivo !=null) || $this->archivo != "")
        {   
            $nombreArchivo = rand (0, 999) . $this->archivo->getClientOriginalName();
            $urlArchivo = "/storage/permisos-PDF/". $nombreArchivo;
            $this->archivo->storeAs('public/permisos-PDF' ,$nombreArchivo);
        }else{
            echo "Hubo un error";
        }

        $permiso = Permiso::find ($idePermiso);
        
        $permiso->archivo_adjunto = $urlArchivo;
        $permiso->estado_id = 3;
        $permiso->save();


        session()->flash('mensajeOk', 'Archivo cargado y tramite cerrado.');
    }
}
