<?php

namespace App\Http\Livewire\Recursoshumanos;

use Livewire\Component;
use App\Models\Licencia;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class LicenciasAprobadasComponent extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $paginationTheme = "bootstrap";

    //Ver tabla estados.
    public $estado = 2;
    public $titulo = "Licencias Aprobadas";
    public $archivo;

    public function render()
    {
        $licencias = Licencia::Where('estado_id', $this->estado)->latest()->Paginate(20);
       
        return view('livewire.recursoshumanos.licencias-aprobadas-component',['tramites'=>$licencias]);
    }

    public function cerradas()
    {
        $this->estado = 3;
        $this->titulo = "Licencias Cerradas";
    }

    public function aprobadas()
    {
        $this->estado = 2;
        $this->titulo = "Licencias Aprobadas";
    }

    function subirArchivoLicencia ($ideLicencia)
    {
        $this->validate(
            [
            'archivo' => 'required|mimes:pdf|max:5000', // 5MB Max
        ]);
           
        if (($this->archivo !=null) || $this->archivo != "")
        {   
            $nombreArchivo = rand (0, 999) . $this->archivo->getClientOriginalName();
            $urlArchivo = "/storage/licencias-PDF/". $nombreArchivo;
            $this->archivo->storeAs('public/licencias-PDF' ,$nombreArchivo);
        }else{
            echo "Hubo un error";
        }

        $licencia = Licencia::find ($ideLicencia);
        
        $licencia->archivo_adjunto = $urlArchivo;
        $licencia->estado_id = 3;
        $licencia->save();


        session()->flash('mensajeOk', 'Archivo cargado y tramite cerrado.');
    }
}
