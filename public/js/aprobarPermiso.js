let aprobarPermiso = async (idPermiso)=>
{
    console.log ("El resultado es" + idPermiso)
    if (confirm("¿Desea aprobar el trámite? Esta operación no puede deshacerse."))
    {
        const url = '/aprobarPermiso'
        const request = new Request(url, 
        {
            method: 'POST',
            body: JSON.stringify({"idPermiso": idPermiso}),
            cache: "no-cache",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        
        });
        const response = await fetch(request);
        const data = await response.json();       
        console.log(data)
        Swal.fire(
            {
                type: 'success',
                title: data,
                showConfirmButton: false,
                timer: 2000
            })
        
    }
    location.reload()
}
