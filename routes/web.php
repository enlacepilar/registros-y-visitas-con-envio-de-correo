<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IngresoController;
use App\Http\Controllers\AdministradorController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\GeneraPDFController;
use App\Http\Controllers\RecursosHumanosController;
use App\Http\Controllers\EstadoController;
use App\Http\Controllers\RolController;
use App\Http\Controllers\DepartamentoController;

use App\Http\Controllers\PHPMailerController;

use Illuminate\Support\Facades\DB;

#Antes de ingresar
Route::get('migrar', function () {
    return view('migrar');
});
Route::get('/crear-roles', [RolController::class, 'crearRoles']);
Route::get('/crear-estados', [EstadoController::class, 'crearEstados']);
Route::get('/crear-departamentos', [DepartamentoController::class, 'crearDepartamentos']);

#Al ingresar
Route::get('/', [IngresoController::class, 'ingreso'])->middleware('auth');

#Enviar correos
Route::get("email", [PHPMailerController::class, "email"])->name("email");
Route::post("send-email", [PHPMailerController::class, "composeEmail"])->name("send-email");
 

#section Del usuario
Route::get('/inicio', [UsuarioController::class, 'inicio'])->middleware('auth');
    Route::get('/datosUsuario', [UsuarioController::class, 'datosUsuario'])->middleware('auth');
    Route::post('/modificaUsuario', [UsuarioController::class, 'modificaUsuario'])->middleware('auth');

    #Mis tramites -- ver como desplegarlas si todas juntas o por separado
    Route::get('/misLicencias', [LicenciaController::class, 'misLicencias'])->middleware('auth');
    Route::get('/misPermisos', [PermisoController::class, 'misPermisos'])->middleware('auth');
#endsection

#section Usuarios Externos
    Route::get('/externos', function () {
        return view ('externos.turnos');
    })->middleware('auth');

    #aprobar Licencia con marca de agua --en preparacion
    Route::get('aprobar/', function () {
        $tramites = DB::table('licencias as lic')
            ->join('users AS usu', 'lic.id_usuario', '=', 'usu.id')
            ->select('lic.id AS idLicencia', 'lic.tipo_tramite AS tramite', 'lic.sector_pertenencia AS sector', 'lic.fecha_tramite',
            'lic.licencia_desde', 'lic.licencia_hasta', 'lic.archivo_adjunto', 
            'usu.name AS nombre', 'usu.email AS correo', 'usu.socio AS socio')
            ->get();

            return view ('admin.licencia.aprobar-licencia', ['tramites'=> $tramites]);
        
    });
    Route::get('/aprobarLicencia/{ide}', [LicenciaController::class, 'aprobarLicenciaExterno']);
#endsection

#section Del Administrador
    Route::get('/adminDatosUsuario', [AdministradorController::class, 'adminDatosUsuario'])->middleware('auth');
    Route::get('/recibeDatosUsuarioAdmin', [AdministradorController::class, 'recibeDatosUsuarioAdmin'])->middleware('auth');
    Route::get('/buscaUsuario', [AdministradorController::class, 'buscaUsuario'])->middleware('auth');
    //Route::post('/editaUsuAdmin/{ide}', [AdministradorController::class, 'editaUsuAdmin'])->middleware('auth');//esto es para un solo resUltado, cuando quiero ir a editarlo
    Route::get('nuevo_usuario', function () {
        return view ('admin.nuevo-usuario');
    })->middleware('auth');

    Route::get('sectores', function () {
        return view ('admin.departamentos');
    })->middleware('auth');

    Route::get('/recibe_nuevo_usuario', [AdministradorController::class, 'recibeNuevoUsuario'])->middleware('auth');
    Route::get("/modificaUsuarioAdmin", [AdministradorController::class, 'modificaUsuarioAdmin'])->middleware('auth');
    Route::post("/eliminaUsuario", [AdministradorController::class, 'eliminaUsuario'])->middleware('auth');
    //Route::get('/adminTramites', [AdministradorController::class, 'adminTramites'])->middleware('auth');

   
#endsection

#section De Recursos Humanos
    #La primera vez traigo los datos con un controlador. La siguiente a traves de un componente livewire
    Route::get("/rrhhLicencias", [RecursosHumanosController::class, 'licencias'])->middleware('auth');
    Route::get("/rrhhPermisos", [RecursosHumanosController::class, 'permisos'])->middleware('auth');
#endsection
require __DIR__.'/auth.php';
